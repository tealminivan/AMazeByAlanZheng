package edu.wm.cs.cs301.AlanZheng.generation;

import java.util.ArrayList;

/**
 * The maze is built with Kruskal's algorithm.
 * The algorithm grabs random segments of the maze at random and carves it out
 * by removing the walls between a segment and its neighbor.
 * 
 */

public class MazeBuilderKruskal extends MazeBuilder implements Runnable{
	
	
	public MazeBuilderKruskal() {
		super();
		System.out.println("MazeBuilderKruskal uses Kruskal's algorithm to generate maze.");
	}
	
	public MazeBuilderKruskal(boolean det) {
		super(det);
		System.out.println("MazeBuilderKruskal uses Kruskal's algorithm to generate maze.");
		
	}
	
	/**
	 * This method chooses a random wall that is part of the list.
	 * The coordinates of the wall's location are then retrieved
	 * along with the coordinates of the retrieved location's neighbor.
	 * The method then translates the coordinates into the grid structure
	 * in order to get the ids of the location. 
	 * If the ids of the location and its neighbor do not match
	 * the method merges the cells together to create a "set".
	 */
	
	@Override
	protected void generatePathways() {
		
		final ArrayList<Wall> candidates = new ArrayList<Wall>();
		updateListOfWalls(candidates);
		//get a array from the setGrid method
		int[][] grid = setGrid();
		
		while(!candidates.isEmpty()){                                                 
			Wall wall = extractWallFromCandidateSetRandomly(candidates); 
			//get current coordinates from the walls
			int x = wall.getX();                                     
			int y = wall.getY();      
			//get neighbor coordinates from the walls
			int nx = wall.getNeighborX();                            
			int ny = wall.getNeighborY();	
			//temporary variables to hold specific values for the neighbor and current
			int holdNIndex = grid[nx][ny];
			int holdIndex = grid[x][y];
			//condition to make sure the current position does not share the value as its neighbor
			if (grid[x][y] != grid[nx][ny]){   
				//delete the wall at the current spot
				cells.deleteWall(wall);
				//loop to update the grid after deletion 
				for (int i = 0; i < width; i++){		
					for (int j = 0; j < height; j++){
						if (grid[i][j] == holdNIndex){
							//changes the value of the neighbor to the value of the current tile
							grid[i][j] = holdIndex;
						}
					}
				}
			}
		}		
		
	}
	/**
	 * Method is used to provide each cell in the maze an id value using a 2d array data structure
	 * 
	 * @return values of the grid with the cells that have values
	 */
	private int[][] setGrid() {
		//create a new grid 
		int[][] grid = null;
		grid = new int[width][height];
		int index = 0; 						
		//assigns each tile of a grid a number based on the width and height of the maze
		for (int i = 0; i < width; i++){		
			for (int j = 0; j < height; j++){
				grid[i][j] = index;
				index++;
			}
		}
		
		return grid;
	}
	
	/**
	 * Pick a random position in the list of candidates, remove the candidate from the list and return it
	 * @param candidates
	 * @return candidate from the list, randomly chosen
	 */
	private Wall extractWallFromCandidateSetRandomly(final ArrayList<Wall> candidates) {
		
		return candidates.remove(random.nextIntWithinInterval(0, candidates.size()-1)); 
	}
	
	/**
	 * Updates the list of walls that could be removed from the maze based on walls towards new cells
	 * This method differs from the one in Prim's where it checks valid walls from the entire grid.
	 * @param walls
	 */
	protected void updateListOfWalls(ArrayList<Wall> walls) {
		//nested for loop needed to check all the walls through out the entire board
		//add walls that are valid for removal
		for(int i = 0; i < width; i++) {
			for(int j =0; j < height; j++) {
				for (CardinalDirection cd : CardinalDirection.values()) {
					Wall wall = new Wall(i, j, cd);
					if(cells.canGo(wall)) {
						walls.add(wall);
					}
				}
				
			}
		}
	}
	
	

}
