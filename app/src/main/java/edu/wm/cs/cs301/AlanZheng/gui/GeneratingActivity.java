package edu.wm.cs.cs301.AlanZheng.gui;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.logging.Logger;

import edu.wm.cs.cs301.AlanZheng.R;
import edu.wm.cs.cs301.AlanZheng.generation.DummyOrder;
import edu.wm.cs.cs301.AlanZheng.generation.MazeConfiguration;
import edu.wm.cs.cs301.AlanZheng.generation.MazeFactory;
import edu.wm.cs.cs301.AlanZheng.generation.Order;

public class GeneratingActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private TextView loadingText;
    private TextView driverText;
    private TextView builderText;

    private Order.Builder builderConstant;

    private int progressStatus = 0;

    private Handler handler = new Handler();
    String driver = "";
    String builder ="";
    int skillLevel = 0;

    private volatile boolean cancel = false;

    MazeFactory mf;
    DummyOrder ord;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        loadingText = (TextView) findViewById(R.id.LoadingCompleteTextView);
        driverText = (TextView) findViewById(R.id.driver);
        builderText = (TextView) findViewById(R.id.builder);

        // receive the values from the AMazeActivity class
        Intent launch = getIntent();
        driver = launch.getStringExtra("driver");
        driverText.setText(driver);
        builder = launch.getStringExtra("builder");
        builderText.setText(builder);
        skillLevel = launch.getIntExtra("skillLevel", 0);

        //get the builder strings and change them into constants
        if(builder.equalsIgnoreCase("DFS")){
            this.builderConstant = Order.Builder.DFS;
        }else if(builder.equalsIgnoreCase("Prim")){
            this.builderConstant = Order.Builder.Prim;
        }else if(builder.equalsIgnoreCase("Kruskal")){
            this.builderConstant = Order.Builder.Kruskal;
        }

        //load the maze
        mf = new MazeFactory();
        ord = new DummyOrder(builderConstant, skillLevel);
        mf.order(ord);




        //dummy loading process to mimic the loading to generate the maze
        new Thread(new Runnable() {
            public void run(){
                while (progressStatus < 100){
                    progressStatus++;
                    android.os.SystemClock.sleep(50);
                    mf.waitTillDelivered();

                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressStatus);
                        }
                    });
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        // shows the loading finished text when progress bar is done

                        loadingText.setVisibility(View.VISIBLE);

                        // condition to change activity to either Manual or Animation depending on what was chosen
                        if (cancel == false) {
                            if (new String(driver).equals("Manual")) {
                                Global.config = ord.getMazeConfiguration();
                                changeToManual();
                            }
                            else {
                                Global.config = ord.getMazeConfiguration();
                                changeToAnimation();
                            }

                        }

                    }
                });
            }
        }).start();
    }

    /**
     *  Method to change the activity to manual after the loading is finished
     */

    public void changeToManual(){
        Intent i = new Intent(this, PlayManuallyActivity.class);
        i.putExtra("driver", driver);
        i.putExtra("builder", builder);
        i.putExtra("skillLevel", skillLevel);
        startActivity(i);
    }

    /**
     *  Method to change the activity to animation after the loading is finished
     */
    public void changeToAnimation(){
        Intent i = new Intent(this, PlayAnimationActivity.class);

        startActivity(i);
    }

    /**
     *  Method to have the back button to go back to the main activity
     */
    public void onBackPressed(){
        Intent i = new Intent(this, AMazeActivity.class);
        startActivity(i);
        cancel = true;
        mf.cancel();
        finish();
    }
}
