package edu.wm.cs.cs301.AlanZheng.generation;

/**
 * Stub Class for order interface 
 * Used to make tests for MazeFactoryTest.java
 * 
 */

public class DummyOrder implements Order {
	
	private int skillLevel;
	private Builder builder;
	private boolean perfect;
	private int percentDone;
	private MazeConfiguration mzConfig;
	
	public DummyOrder(Builder builder, int skillLevel) {

		this.skillLevel = skillLevel;
		this.perfect = perfect;
		//allow to change the parameter for the type of maze generating algorithm
		this.builder = builder;
		
	}
	

	@Override
	public int getSkillLevel() {
		return skillLevel;
	}

	@Override
	public Builder getBuilder() {
		return builder;
	}

	@Override
	public boolean isPerfect() {
		return perfect;
	}


	@Override
	public void deliver(MazeConfiguration mazeConfig) {
		this.mzConfig = mazeConfig;

	}

	@Override
	public void updateProgress(int percentage) {
		this.percentDone = percentage;

	}
	
	public MazeConfiguration getMazeConfiguration() {
		return mzConfig;
	}

}
