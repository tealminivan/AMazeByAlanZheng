package edu.wm.cs.cs301.AlanZheng.gui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import edu.wm.cs.cs301.AlanZheng.R;

public class AMazeActivity extends AppCompatActivity {

    SeekBar seekBar;
    String driver = "";
    String builder = "";
    Spinner spinner;
    Spinner spinner2;
    int progress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amaze);
        //populating the builder spinner with the algorithm strings
        Spinner spinner = (Spinner) findViewById(R.id.builderSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.builder, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        //populating the driver spinner with the driver strings
        Spinner spinner2 = (Spinner) findViewById(R.id.driverSpinner);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,R.array.driver, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        SeekBar seekBar = (SeekBar) findViewById(R.id.levelSeekBar);
        Button start = (Button) findViewById(R.id.explorebutton);

        // Listener for seekBar
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progress = i;
                // pop up on android to show which skill level is chosen
                Toast.makeText(getApplicationContext(), String.valueOf(i), Toast.LENGTH_LONG).show();
                // message in logcat to show which skill level is chosen
                Log.v("SKill Level: ", String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

       // Listener for builder spinner
       spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               builder = adapterView.getSelectedItem().toString();
               // message in logcat to see which builder is chosen
               Log.v("builder: ", builder);
           }

           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {

           }
       });




        // Listener for driver spinner in order to pass the value to the generating activity
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                driver = adapterView.getSelectedItem().toString();
                // message in logcat to see which driver is chosen
                Log.v("driver: ", driver);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



    /**
     * Changes the activity from AMazeActivity to GeneratingActivity
     */
    public void changeToGenerating(View view){
        Intent i = new Intent(this, GeneratingActivity.class);
        //pass over values for driver chosen
        i.putExtra("driver", driver);
        //pass over values for builder chosen
        i.putExtra("builder", builder);
        i.putExtra("skillLevel", progress);
        startActivity(i);
    }

    /**
     * Goes through the file system and gets the maze level based on the level that is chosen
     */
    public void revisit(View view){
        // receives the skill level from the listener
        int mazeLevel = progress;
        String builderChosen = builder;
        String filename = "Maze_level_" + mazeLevel + "_" + builderChosen + ".xml";
        // create the file path
        File file = getApplicationContext().getFileStreamPath(filename);

        // condition that either displays that a file is not found or it goes to the next activity
        if (!file.exists()){
            Toast.makeText(getApplicationContext(), "Maze file does not exist", Toast.LENGTH_LONG).show();
        }
        else{
            Log.v("Reading file :", filename);
            Intent i = new Intent(this, GeneratingActivity.class);
            //pass over values for driver chosen
            i.putExtra("driver", driver);
            //pass over values for builder chosen
            i.putExtra("builder", builder);
            //pass over the filename
            i.putExtra("filename", filename);
            startActivity(i);
        }
    }

    public void onBackPressed(){
        Intent i = new Intent(this, AMazeActivity.class);
        startActivity(i);
        finish();
    }


    }


