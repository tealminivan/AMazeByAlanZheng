package edu.wm.cs.cs301.AlanZheng.gui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import edu.wm.cs.cs301.AlanZheng.R;

public class FinishManualActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_manual);
    }

    /**
     *  Method to have the back button to go back to the main activity
     */
    public void onBackPressed(){
        Intent i = new Intent(this, AMazeActivity.class);
        startActivity(i);
        finish();
    }
}
