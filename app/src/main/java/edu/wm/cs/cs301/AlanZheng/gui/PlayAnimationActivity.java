package edu.wm.cs.cs301.AlanZheng.gui;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.wm.cs.cs301.AlanZheng.R;
import edu.wm.cs.cs301.AlanZheng.mazegui.Constants;
import edu.wm.cs.cs301.AlanZheng.mazegui.MazePanel;
import edu.wm.cs.cs301.AlanZheng.mazegui.StatePlaying;

public class PlayAnimationActivity extends AppCompatActivity {

    MazePanel panel;
    StatePlaying state;
    RelativeLayout layout;

    private boolean paused = false;
    private Button pause;
    private TextView pausetext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_animation);

        pause = (Button) findViewById(R.id.pause_button);
        pausetext = (TextView) findViewById(R.id.pause_text);

        state = new StatePlaying();
        state.setMazeConfiguration(Global.config);

        layout = (RelativeLayout)findViewById(R.id.playAnimation);
        panel = new MazePanel(this);
        state.start(panel);


        final Resources resource = this.getResources();
        runOnUiThread(new Runnable() {
            public void run() {
                Bitmap bitMap = panel.getBitMap();
                Drawable background = new BitmapDrawable(resource, bitMap);
                layout.setBackground(background);
            }
        });


    }


    /**
     *  Method to have the back button to go back to the main activity
     */
    public void onBackPressed(){
        Intent i = new Intent(this, AMazeActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * This method creates a option menu
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.playanimation, menu);
        return true;
    }

    /**
     * Sees which option was chosen
     */
    public boolean onOptionsItemSelected(MenuItem item) {

        //condition that checks the check box if unchecked, and unchecks if checked
        if(item.isChecked()){
            item.setChecked(false);
            if (item.toString().equals("Show Map"))
                state.keyDown(Constants.UserInput.ToggleLocalMap,0);
            else if (item.toString().equals("Show Solution"))
                state.keyDown(Constants.UserInput.ToggleSolution,0);
            else if (item.toString().equals("Show Visible Walls"))
                state.keyDown(Constants.UserInput.ToggleFullMap,0);

            Log.v("Unchecked: ", item.toString());
        }
        else {
            item.setChecked(true);
            if (item.toString().equals("Show Map"))
                state.keyDown(Constants.UserInput.ToggleLocalMap,0);
            else if (item.toString().equals("Show Solution"))
                state.keyDown(Constants.UserInput.ToggleSolution,0);
            else if (item.toString().equals("Show Visible Walls"))
                state.keyDown(Constants.UserInput.ToggleFullMap,0);

            Log.v("Checked: ", item.toString());
        }

        return super.onOptionsItemSelected(item);

    }
    /**
     * Method that goes to the finishing activity for animation if wallfollower or wizard is chosen
     */
    public void changeTofinish(View view){
        Intent i = new Intent(this, FinishActivity.class);
        startActivity(i);
    }

    /**
     * This method pauses the animation when the pause button is pressed
     * also plays when the animation is paused
     */
    public void toPause(View view){
        Log.v("Pause Button", "toggled");
        //displays the text on the button depending on if the animation is paused or not
        if (paused == false){
            pausetext.setText("Paused");
            pause.setText("Play");
            paused = true;
        }
        else{
            pausetext.setText("Play");
            pause.setText("Paused");
            paused = false;


        }
    }
}
