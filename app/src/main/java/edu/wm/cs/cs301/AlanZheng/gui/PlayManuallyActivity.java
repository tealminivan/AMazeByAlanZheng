package edu.wm.cs.cs301.AlanZheng.gui;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import edu.wm.cs.cs301.AlanZheng.R;
import edu.wm.cs.cs301.AlanZheng.generation.DummyOrder;
import edu.wm.cs.cs301.AlanZheng.generation.MazeConfiguration;
import edu.wm.cs.cs301.AlanZheng.generation.MazeFactory;
import edu.wm.cs.cs301.AlanZheng.generation.Order;
import edu.wm.cs.cs301.AlanZheng.mazegui.Constants;
import edu.wm.cs.cs301.AlanZheng.mazegui.MazePanel;
import edu.wm.cs.cs301.AlanZheng.mazegui.StatePlaying;

public class PlayManuallyActivity extends AppCompatActivity {

    MazePanel panel;
    StatePlaying state;
    RelativeLayout layout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually);


        state = new StatePlaying();
        state.setMazeConfiguration(Global.config);

        layout = (RelativeLayout)findViewById(R.id.playManually);
        panel = new MazePanel(this);
        state.start(panel);


        final Resources resource = this.getResources();

        runOnUiThread(new Runnable() {
            public void run() {
                Bitmap bitMap = panel.getBitMap();
                Drawable background = new BitmapDrawable(resource, bitMap);
                layout.setBackground(background);
            }
        });



    }

    /**
     *  Method to have the back button to go back to the main activity
     */
    public void onBackPressed(){
        Intent i = new Intent(this, AMazeActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * This method creates a option menu
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.playmanual, menu);
        return true;
    }

    /**
     * Sees which option was chosen
     */
    public boolean onOptionsItemSelected(MenuItem item) {


        //condition that checks the check box if unchecked, and unchecks if checked
        if(item.isChecked()){
            item.setChecked(false);
            if (item.toString().equals("Show Map"))
                state.keyDown(Constants.UserInput.ToggleLocalMap,0);
            else if (item.toString().equals("Show Solution"))
                state.keyDown(Constants.UserInput.ToggleSolution,0);
            else if (item.toString().equals("Show Visible Walls"))
                state.keyDown(Constants.UserInput.ToggleFullMap,0);

            Log.v("Unchecked: ", item.toString());
        }
        else {
            item.setChecked(true);
            if (item.toString().equals("Show Map"))
                state.keyDown(Constants.UserInput.ToggleLocalMap,0);
            else if (item.toString().equals("Show Solution"))
                state.keyDown(Constants.UserInput.ToggleSolution,0);
            else if (item.toString().equals("Show Visible Walls"))
                state.keyDown(Constants.UserInput.ToggleFullMap,0);

            Log.v("Checked: ", item.toString());
        }

        return super.onOptionsItemSelected(item);

        }

    /**
     * Method that goes to the finishing activity for manual if manual is chosen
     */
    public void changeTofinishmanual(){
        Intent i = new Intent(this, FinishManualActivity.class);
        startActivity(i);
    }



    /**
     *  Move forward
     */
    public void goForward(View view){
        Log.v("Move: ", "forward");
        if (state.isOutside(state.px, state.py)){
            changeTofinishmanual();
        }
        else {
            state.keyDown(Constants.UserInput.Up, 0);
        }
    }

    /**
     *  Move backward
     */
    public void goBackward(View view){
        Log.v("Move: ", "backward");
            if (state.isOutside(state.px, state.py)){
                changeTofinishmanual();
            }
            else{
                state.keyDown(Constants.UserInput.Down, 0);
            }
    }

    /**
     *  Move left
     */
    public void goLeft(View view){
        Log.v("Move: ", "left");
        state.keyDown(Constants.UserInput.Left, 0);
    }

    /**
     *  Move right
     */
    public void goRight(View view){
        Log.v("Move: ", "right");
        state.keyDown(Constants.UserInput.Right, 0);
    }


}
